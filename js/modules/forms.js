import {closeModel, openModel} from './modal';

export function forms() {
    //AJAX Forms
    //задача: взять несколько форм на сайте и отправить данные из форм в файл server.php
    const forms = document.querySelectorAll('form');

    const message = {
        loading: '/img/form/spinner.svg',
        success: 'Спасибо! Скоро мы с Вами свяжемся',
        failure: 'Что-то пошло не так...'
    }

    forms.forEach(item => {
        bindPostData(item);
    });

    const postData = async (url, data) => {  //функция настраивает запрос, посылает его, ожидает ответ от сервера и после трансформирует ответ в json. Это асинхронный код. Функция fetch ничего не будет получать. Поэтому надо дождаться результата промиса. Воспользуемся async await
        const res = await fetch(url, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: data
        });

        return await res.json();  //здесь возвращается промис и его окончания работы нужно дождаться
    };

    function bindPostData(form) {
        form.addEventListener('submit', (e) => {
            e.preventDefault(); //отменяем стандартное поведение браузера - перезагрузку страницы после нажатия кнопки submit

            const statusMessage = document.createElement('img');
            statusMessage.src = message.loading; //создали изображение и подставили атрибут src из const message loading
            statusMessage.style.cssText = `
                display: block;
                margin: 0 auto;
            `;
            form.append(statusMessage);
            form.insertAdjacentElement('afterend', statusMessage); //вставляем спиннер после формы

            //Для JSON нужен заголовок
            const formData = new FormData(form);  //объект, кот позволяет быстро сформировать данные, полученные из формы в формате ключ - значение

            //есть объект специфический FormData, который нужно преобразовать в JSON. Просто так сделать это нельзя, а можно:
            const json = JSON.stringify(Object.fromEntries(formData.entries())); //получаем массив с массивами, после этого в норм объект, а после в JSON

            postData('  http://localhost:3000/requests', json)
                .then(data => {
                    console.log(data);
                    showThanksModal(message.success); //показывается мод окно и через 4 сек оно будет закрываться вмсете с возвращением норм контента
                    form.reset(); //после успешной отправки очищаем форму
                    statusMessage.remove();
                }).catch(() => {
                showThanksModal(message.failure);
            }).finally(() => {
                form.reset();
            });
        });
    }

    function showThanksModal(message) {   //функция, скрывающая модальное окно и показывающая сообщение об успешной отправке данных
        const prevModalDialog = document.querySelector('.modal__dialog');

        prevModalDialog.classList.add('hide'); //скрыли модальное окно
        openModal();

        const thanksModal = document.createElement('div');
        thanksModal.classList.add('modal__dialog');
        thanksModal.innerHTML = `
            <div class="modal__content">
                <div class="modal__close" data-close>×</div>
                <div class="modal__title">${message}</div> 
                <!-- то сообщение, которое в массиве message -->
            </div>
        `;

        document.querySelector('.modal').append(thanksModal);
        setTimeout(() => {
            thanksModal.remove();
            prevModalDialog.classList.add('show');
            prevModalDialog.classList.remove('hide');
            closeModal();
        }, 4000);
    }

    fetch('http://localhost:3000/menu')//копируем из терминала после запуска json-server db.json
        .then(data => data.json())
        .then(res => console.log(res));  //получаем данные из файла db.json

    //Fetch API
    // fetch('https://jsonplaceholder.typicode.com/posts', {
    //     method: "POST",
    //     body: JSON.stringify({name: 'Alex'}),
    //     headers: {
    //         'Content-type': 'application/json'
    //     }
    // })
    //         .then(response => response.json())  //что-то приодит от сервера. превращает json в обычный объект promice
    //         .then(json => console.log(json));
}
