export function slider() {
    //Slider
    const slides = document.querySelectorAll('.offer__slide'), //сами картинки
        prev = document.querySelector('.offer__slider-prev'),  //стрелка назад
        next = document.querySelector('.offer__slider-next'),  //стрелка вперед
        total = document.querySelector('#total'),   //всего слайдов
        current = document.querySelector('#current');  //номер текущего слайда

    //для второго варианта слайдера:
    const slideWrapper = document.querySelector('.offer__slider-wrapper'),  //Обертка картинки
        slidesField = document.querySelector('.offer__slider-inner'),  //блок с внутренним содержанием (картинки)
        width = window.getComputedStyle(slideWrapper).width; //получаем только ширину обертки слайда

    //Navigation for slider
    const slider = document.querySelector('.offer__slider');

    let slideIndex = 1; //let так как переменная в дальнейшем будет меняться

    //LIGHT SLIDER:
    //
    // showSlides(slideIndex); //изначально слайд показывает первую картинку
    //
    // if (slides.length < 10) {
    //     total.textContent = `0${slides.length}`;  //показ общего количества слайдов
    // } else {
    //     total.textContent = slides.length;
    // }
    //
    // function showSlides(n) {   //n - слайд индекс
    //     if (n > slides.length) {
    //         slideIndex = 1; //если n > количество слайдов (перещелкали), то переходим на первый слайд
    //     }
    //     if (n < 1) {
    //         slideIndex = slides.length; //если номер слайда меньше 1, то переходим на последний слайд
    //     }
    //     slides.forEach(item => item.style.display = 'none'); //все слайды скрыты
    //     slides[slideIndex - 1].style.display = 'block'; //показываем выбранный слайд
    //
    //     if (slides.length < 10) {
    //         current.textContent = `0${slideIndex}`;  //показ номера текущего слайда
    //     } else {
    //         current.textContent = slideIndex[slideIndex - 1];
    //     }
    //
    // }
    //
    // function plusSlides(n) {
    //     showSlides(slideIndex += n);  //вызываем функцию показа слайдов и передаем в параметрах индекс слайда. Если +1, то будет показываться след слайд, если -1, то slideIndex = slideIndex - 1
    // }
    //
    // prev.addEventListener('click', () => {
    //     plusSlides(-1);   //обработчик события на нажатие на стрелку "пред. слайд".
    // });
    //
    // next.addEventListener('click', () => {
    //     plusSlides(1);   //обработчик события на нажатие на стрелку "след. слайд".
    // });


    //HARD SLIDER:
    let offset = 0; //отступ

    if (slides.length < 10) {
        total.textContent = `0${slides.length}`;  //показ общего количества слайдов
        current.textContent = `0${slideIndex}`;
    } else {
        total.textContent = slides.length;
        current.textContent = slideIndex;
    }

    slidesField.style.width = 100 * slides.length + '%';  //так мы сможем поместить все слайды на странице в один блок
    slidesField.style.display = 'flex';  //делаем, чтобы все картинки располагались в ряд
    slidesField.style.transition = '0.5s all';

    slideWrapper.style.overflow = 'hidden';  //показываем только одну картинку

    slides.forEach(slide => {
        slide.style.width = width; //каждому слайду устанавливаем одну и ту же ширину
    });

    slider.style.position = 'relative';  //все элементы, кот спозиционированы абсолютно, теперь будут нормально отображаться

    const indicators = document.createElement('ol'), //обертка для всех точек-индикаторов
        dots = []; //массив всех индикаторов

    indicators.classList.add('carousel-indicators');  //обертка для индикаторов
    indicators.style.cssText = `
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 15;
        display: flex;
        justify-content: center;
        margin-right: 15%;
        margin-left: 15%;
        list-style: none;
    `;
    slider.append(indicators); //помещаем обертку индикаторов внутрь слайдера

    for (let i = 0; i < slides.length; i++) {  //создаем столько точек, сколько слайдов
        const dot = document.createElement('li');
        dot.setAttribute('data-slide-to', i + 1); //выставляем каждой точке атрибут с соответствующим номером слайда
        dot.style.cssText = `
            box-sizing: content-box;
            flex: 0 1 auto;
            width: 30px;
            height: 6px;
            margin-right: 3px;
            margin-left: 3px;
            cursor: pointer;
            background-color: #fff;
            background-clip: padding-box;
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent;
            opacity: .5;
            transition: opacity .6s ease;
        `;
        if (i == 0) {   //первая точка будет ярче
            dot.style.opacity = '1';
        }
        indicators.append(dot); //помещаем точки в обертку индикаторов
        dots.push(dot); //помещаем точку в массив
    }

    function getZeroSlides(slides) {
        if (slides.length < 10) {
            return current.textContent = `0${slideIndex}`; //если слайдов меньше 10, то славим ноль перед номером слайда
        } else {
            return current.textContent = slideIndex; //если слайдов > 10, то не ставим ноль перед номером слайда
        }
    }

    function dotsOpacity() {
        dots.forEach(dot => dot.style.opacity = '.5'); //изначально у каждой точки прозрачность 50%
        dots[slideIndex - 1].style.opacity = '1';  //точка номер = текущего слайда ярче
    }

    function deleteNotDigits(str) {
        return +str.replace(/\D/g, '');
    }

    //нажатие на стрелку вправо
    next.addEventListener('click', () => {  //здесь проверяем на последний слайд
        if (offset === deleteNotDigits(width) * (slides.length - 1)) {  //изначально width - это напрмер "500px". Превращаем ее в число (+) и все не числа заменяем на ''. Таким образом, получаем только цифры.
            offset = 0;   //если последний слайд
        } else {  //если не последний слайд
            offset += deleteNotDigits(width)
        }

        slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

        if (slideIndex == slides.length) {  //значит мы дошли до конца слайдера
            slideIndex = 1  //перемещаемся на первую позицию
        } else {
            slideIndex++;
        }

        getZeroSlides(slides);
        dotsOpacity();

    });

    //нажатие на стрелку влево
    prev.addEventListener('click', () => {  //здесь проверяем на последний слайд
        if (offset == 0) {  //если первый слайд
            offset = deleteNotDigits(width) * (slides.length - 1); //изначально width - это напрмер "500px". Превращаем ее в число (+) и отрезаем "px"
        } else {
            offset -= deleteNotDigits(width)
        }

        slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

        if (slideIndex == 1) {  //когда мы находимся на первом слайде, клие на кнопку предыдущего слайда будет смещать нас на последний слайд
            slideIndex = slides.length
        } else {
            slideIndex--;  //когда нажимаем на кнопку предыдущий слайд, то показываем предыдущий слайд
        }

        getZeroSlides(slides);
        dotsOpacity();
    });

    //добавляем возмоность попадать на нужный слайд, нажимая на индикаторы
    dots.forEach(dot => {
        dot.addEventListener('click', (e) => {
            const slideTo = e.target.getAttribute('data-slide-to');

            slideIndex = slideTo;
            offset = deleteNotDigits(width) * (slideTo - 1);
            slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

            getZeroSlides(slides);
            dotsOpacity();
        });
    });

}
