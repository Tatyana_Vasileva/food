export function openModal() {
    modal.classList.add('show');
    modal.classList.remove('hide');
    document.body.style.overflow = 'hidden'; //убираем вертикальный скроллинг на странице, так как открыто мод окно
    clearInterval(modalTimerId)  //если пользователь сам открыл мод окно, то оно не должно ему показываться через несколько секунд. Поэтому сбрасываем счетчик
}

export function closeModal() {
    modal.classList.add('hide');
    modal.classList.remove('show');
    document.body.style.overflow = ''; //возвращаем странице скроллинг
}

export function modal() {
    //Modal window

    const modalTrigger = document.querySelectorAll('[data-modal]'), //Кнопки "связаться с нами" (псевдомассив кнопок. Далее перебираем фор ичем)
        modal = document.querySelector('.modal');  //само модальное окно
    // modalCloseBtn = document.querySelector('[data-close');  //крестик в мод окне

    modalTrigger.forEach(btn => {
        btn.addEventListener('click', openModal);
    });

    // modalCloseBtn.addEventListener('click', closeModal);

    //показывать и скрывать мод окно через тогл:

    // modalTrigger.addEventListener('click', () => {
    //     modal.classList.toggle('show');
    //     document.body.style.overflow = 'hidden'; //убираем вертикальный скроллинг на странице, так как открыто мод окно
    // });
    //
    // modalCloseBtn.addEventListener('click', () => {
    //     modal.classList.toggle('show');
    //     document.body.style.overflow = ''; //возвращаем странице скроллинг
    // });


    modal.addEventListener('click', (e) => {
        if (e.target === modal || e.target.getAttribute('data-close') == '') {    //если клик на область с классом модал, то закрываем мод окно
            closeModal();
        }
    });

    document.addEventListener('keydown', (e) => {  //закрытие мод окна по нажатию по ESC
        if (e.code === "Escape" && modal.classList.contains('show')) {  //если нажата ESC и модал окно содержит класс шоу, то есть открыто, то:
            closeModal();
        }
    });

    const modalTimerId = setTimeout(openModal, 50000); //если пользователь не открыл мод окно, то оно само откроется через 50 сек

    //чтобы мод окно показывалось после пролистывания страницы вниз до конца:

    function showModalByScroll() {
        if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight){ //берем прокрутку справа + тот контент, кот сейчас видим, сравниваемс полным сайтом. как только они совпадают, то значит клиент долистал до конца
            openModal();
            window.removeEventListener('scroll', showModalByScroll); //чтобы модальное окно открылось только один раз при пролитсывании в конец страницы
        }
    }

    window.addEventListener('scroll', showModalByScroll);
}

