export function cards() {
    //Используем классы для карточек. Шаблонизируем
    class MenuCard {
        constructor(src, alt, title, desc, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.desc = desc;
            this.price = price;
            this.parent = document.querySelector(parentSelector);
            this.classes = classes;
            this.transfer = 70; //курс доллара
            this.changeToRUB();
        }

        changeToRUB() {
            this.price = this.price * this.transfer
        }

        render() {
            const element = document.createElement('div');
            if (this.classes.length === 0) {   //если у элемента нет массива с классами, то добавляем класс по умолчанию menu__item
                this.element = 'menu__item';
                element.classList.add(this.element);
            } else {
                this.classes.forEach(className => element.classList.add(className));  //Иначе каждый имеющийся в массиве классов класс добавляем к элементу
            }


            this.classes.forEach(className => element.classList.add(className))   //каждый элемент внутри массива это className. Добавляем к элементу каждый новый класс
            element.innerHTML = `
                    <img src=${this.src} alt=${this.alt}>
                    <h3 class="menu__item-subtitle">${this.title}</h3>
                    <div class="menu__item-descr">${this.desc}</div>
                    <div class="menu__item-divider"></div>
                    <div class="menu__item-price">
                        <div class="menu__item-cost">Цена:</div>
                        <div class="menu__item-total"><span>${this.price}</span> руб/день</div>
                    </div>
            `;
            this.parent.append(element);
        }
    }

    const getResource = async (url) => {  //функция настраивает запрос, посылает его, ожидает ответ от сервера и после трансформирует ответ в json. Это асинхронный код. Функция fetch ничего не будет получать. Поэтому надо дождаться результата промиса. Воспользуемся async await
        const res = await fetch(url);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}, status: ${res.status}`);
        }

        return await res.json();  //здесь возвращается промис и его окончания работы нужно дождаться
    };

    // getResource('http://localhost:3000/menu')  //для вывода карточек
    //     .then(data => {
    //        data.forEach(({img, altimg, title, descr, price}) => {
    //          new MenuCard(img, altimg, title, descr, price, '.menu .container').render();
    //        });
    //     });

    // getResource('http://localhost:3000/menu')  //для вывода карточек. функция получает data - массив
    //     .then(data => createCard(data));  //

    axios.get('http://localhost:3000/menu')
        .then(data => {
            data.data.forEach(({img, altimg, title, descr, price}) => {
                new MenuCard(img, altimg, title, descr, price, '.menu .container').render();
            });
        });

    function createCard(data) {
        data.forEach(({img, altimg, title, descr, price}) => {   //массив data перебирается через фор ич. Объекты деструктуризируются на отдельные св-ва
            price *= 70;
            const element = document.createElement('div');

            element.classList.add('menu__item');

            element.innerHTML = ` 
                <img src=${img} alt=${altimg}>
                <h3 class="menu__item-subtitle">${title}</h3>
                <div class="menu__item-descr">${descr}</div>
                <div class="menu__item-divider"></div>
                <div class="menu__item-price"
                    <div class="menu__item-cost">Цена:</div>
                    <div class="menu__item-total"><span>${price}</span> руб/день</div>
                </div>
            `;

            document.querySelector('.menu .container').append(element);
        })
    }

}
