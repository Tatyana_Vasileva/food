export function timer() {
    //Обратный отсчет времени
    const deadline = '2020-12-01';

    //функция разницы между deadline и текущим временем
    function getTimeRemaining(endtime) {
        const t = Date.parse(endtime) - Date.parse(new Date()), //разница в миллисекундах между датой будущего события и текущей датой
            //чтобы получить из t количество дней, делим t на количество миллисекунд в часе и округляем полученное значение:
            days = Math.floor(t / (1000 * 60 * 60 * 24)),
            hours = Math.floor((t / (1000 * 60 * 60) % 24)), //получаем хвостик, которого не хватает до полных суток
            minutes = Math.floor((t / 1000 /60) % 60),  //получаем хвостик, которого не хватает до полного часа
            seconds = Math.floor((t / 1000) % 60);

        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,  //получаем объект
        };
    }

    function getZero(num) {
        if (num >= 0 && num < 10) {
            return `0${num}`;
        } else {
            return num;
        }
    }

    function setClock(selector, endtime) {
        const timer = document.querySelector(selector),
              days = timer.querySelector('#days'),
              hours = timer.querySelector('#hours'),
              minutes = timer.querySelector('#minutes'),
              seconds = timer.querySelector('#seconds'),
              timeInterval = setInterval(updateClock, 1000); //запуск фнкции updateClock через каждую секунду

        updateClock(); //вызываем эту функцию, чтоюы значения дней, часов, сек и миллисек, которые стоят в верстке, не отображались

        function updateClock() {
            const t = getTimeRemaining(endtime);  //сюда записывается результат работы функции getRem.. в качестве объекта

            days.innerHTML = getZero(t.days);
            hours.innerHTML = getZero(t.hours);
            minutes.innerHTML = getZero(t.minutes);
            seconds.innerHTML = getZero(t.seconds);

            if (t.total <= 0) {
                clearInterval(timeInterval); //если время вышло, то таймер не обновляем
            }
        }
    }

    setClock('.timer', deadline);
}
