export function calc() {
    //Калькулятор каллорий
    const result = document.querySelector('.calculating__result span'); //сюда записывается результат подсчета

    let sex, height, weight, age, ratio;

    if (localStorage.getItem('sex')) {   //если в лок хранилище уже есть данные пола, то переменная будет равняться этому значению
        sex = localStorage.getItem('sex');
    } else {          //иначе устанавливаем значения по умолчанию
        sex = 'female';
        localStorage.setItem('sex', 'female');
    }

    if (localStorage.getItem('ratio')) {
        ratio = localStorage.getItem('ratio');
    } else {
        ratio = 1.375;
        localStorage.setItem('ratio', 1.375);
    }

    function initLocalSettings(selector, activeClass) {  //проверяется, какое значение содержится в локальном хранилище и потом кнопка с этим значением становится активной
        const elements = document.querySelectorAll(selector);

        elements.forEach(elem => {
            elem.classList.remove(activeClass);
            if (elem.getAttribute('id') === localStorage.getItem('sex')) {
                elem.classList.add(activeClass);
            }
            if (elem.getAttribute('data-ratio') === localStorage.getItem('ratio')) {
                elem.classList.add(activeClass);
            }
        });
    }

    initLocalSettings('#gender div', 'calculating__choose-item_active');
    initLocalSettings('.calculating__choose_big div', 'calculating__choose-item_active');

    function calcTotal() { //функция рассчета калорий. Должна вызываться всегда, когда пользователь либо что-то выбрал, либо что-то ввел
        if (!sex || !height || !weight || !age || !ratio) {
            result.textContent = '____'; //если какое-то значение не заполнено, то в результат выводится 4 подчеркивания
            return;  //с помощью ретурна мы досрочно прерываем функцию. Т к если функция увидит ретурн, то дальнейшие условия работать не будут
        }

        if (sex === 'female') {
            result.textContent = Math.round((447.6 + (9.2 * weight) + (3.1 * height) - (4.3 * age)) * ratio); //норма каллорий для женщин
        } else  {
            result.textContent = Math.round((88.36 + (13.4 * weight) + (4.8 * height) - (5.7 * age)) * ratio); //норма каллорий для мужчин
        }
    }

    calcTotal();

    function getStaticInformation(selector, activeClass) {  //Selector - это блоки выбора пола и физической активности
        const elements = document.querySelectorAll(selector);

        elements.forEach(elem => {
            elem.addEventListener('click', (e) => {
                if (e.target.getAttribute('data-ratio')) {
                    ratio = +e.target.getAttribute('data-ratio');   //если выбрана активность, то атрибут записывается в переменную ratio
                    localStorage.setItem('ratio', +e.target.getAttribute('data-ratio')); //теперь в локальном хранилище будет сохранятся коэффициент

                } else {
                    sex = e.target.getAttribute('id');
                    localStorage.setItem('sex', e.target.getAttribute('id')); //теперь в локальном хранилище будет сохранятся выбранный пол

                }

                elements.forEach(elem => {
                    elem.classList.remove(activeClass); //сначала класс активности убираему всех элементов
                });
                e.target.classList.add(activeClass); //к выбранному элементу добавляем класс активности

                calcTotal();
            });
        })
    }

    getStaticInformation('#gender div', 'calculating__choose-item_active');
    getStaticInformation('.calculating__choose_big div', 'calculating__choose-item_active');

    function getDynamicInformation(selector) {  //selector - это селектор инпутов, в кот будут записываться значения
        const input = document.querySelector(selector);

        input.addEventListener('input', () => {  //что мы будем делать, когда пользователь что-то вводит

            if (input.value.match(/\D/g)) {   //проверка на введение цифр пользователем
                input.style.border = '1px solid red';
            } else {
                input.style.border = 'none';
            }

            switch(input.getAttribute('id')) {  //смотря куда пользователь вводит значение, будем брать это значение и присваивать соответствующей переменной
                case 'height':
                    height = +input.value;
                    break;
                case 'weight':
                    weight = +input.value;
                    break;
                case 'age':
                    age = +input.value;
                    break;
            }
            calcTotal();
        });
    }
    getDynamicInformation('#height');
    getDynamicInformation('#weight');
    getDynamicInformation('#age');
}
