export function tabs() {
    //Табы или слайдер
    const tabs = document.querySelectorAll('.tabheader__item'),  //элемент меню справа
        tabsContent = document.querySelectorAll('.tabcontent'),  //картинки слайдера
        tabsParent = document.querySelector('.tabheader__items'); //меню справа

    function hideTabContent() {
        tabsContent.forEach(item => {
            // item.style.display = 'none';
            item.classList.add('hide');
            item.classList.remove('show', 'fade');
        });

        tabs.forEach(item => {
            item.classList.remove('tabheader__item_active');
        });
    }

    function showTabContent(i = 0) {
        // tabsContent[i].style.display = "block";
        tabsContent[i].classList.add('show', 'fade');
        tabsContent[i].classList.remove('hide');
        tabs[i].classList.add('tabheader__item_active');
    }

    hideTabContent();
    showTabContent(); //если написать в функции во входных параметрах, например, i = 0, то когда вызываем функцию без аргумента, то по умолчанию будет и=0

    tabsParent.addEventListener('click', (event) => {
        const target = event.target;

        if (target && target.classList.contains('tabheader__item')) {
            tabs.forEach((item, i) => {
                if (target == item) {
                    hideTabContent();
                    showTabContent(i);
                }
            });
        }
    });
}
