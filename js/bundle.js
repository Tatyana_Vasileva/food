/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/modules/calc.js":
/*!****************************!*\
  !*** ./js/modules/calc.js ***!
  \****************************/
/*! exports provided: calc */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calc", function() { return calc; });
function calc() {
    //Калькулятор каллорий
    const result = document.querySelector('.calculating__result span'); //сюда записывается результат подсчета

    let sex, height, weight, age, ratio;

    if (localStorage.getItem('sex')) {   //если в лок хранилище уже есть данные пола, то переменная будет равняться этому значению
        sex = localStorage.getItem('sex');
    } else {          //иначе устанавливаем значения по умолчанию
        sex = 'female';
        localStorage.setItem('sex', 'female');
    }

    if (localStorage.getItem('ratio')) {
        ratio = localStorage.getItem('ratio');
    } else {
        ratio = 1.375;
        localStorage.setItem('ratio', 1.375);
    }

    function initLocalSettings(selector, activeClass) {  //проверяется, какое значение содержится в локальном хранилище и потом кнопка с этим значением становится активной
        const elements = document.querySelectorAll(selector);

        elements.forEach(elem => {
            elem.classList.remove(activeClass);
            if (elem.getAttribute('id') === localStorage.getItem('sex')) {
                elem.classList.add(activeClass);
            }
            if (elem.getAttribute('data-ratio') === localStorage.getItem('ratio')) {
                elem.classList.add(activeClass);
            }
        });
    }

    initLocalSettings('#gender div', 'calculating__choose-item_active');
    initLocalSettings('.calculating__choose_big div', 'calculating__choose-item_active');

    function calcTotal() { //функция рассчета калорий. Должна вызываться всегда, когда пользователь либо что-то выбрал, либо что-то ввел
        if (!sex || !height || !weight || !age || !ratio) {
            result.textContent = '____'; //если какое-то значение не заполнено, то в результат выводится 4 подчеркивания
            return;  //с помощью ретурна мы досрочно прерываем функцию. Т к если функция увидит ретурн, то дальнейшие условия работать не будут
        }

        if (sex === 'female') {
            result.textContent = Math.round((447.6 + (9.2 * weight) + (3.1 * height) - (4.3 * age)) * ratio); //норма каллорий для женщин
        } else  {
            result.textContent = Math.round((88.36 + (13.4 * weight) + (4.8 * height) - (5.7 * age)) * ratio); //норма каллорий для мужчин
        }
    }

    calcTotal();

    function getStaticInformation(selector, activeClass) {  //Selector - это блоки выбора пола и физической активности
        const elements = document.querySelectorAll(selector);

        elements.forEach(elem => {
            elem.addEventListener('click', (e) => {
                if (e.target.getAttribute('data-ratio')) {
                    ratio = +e.target.getAttribute('data-ratio');   //если выбрана активность, то атрибут записывается в переменную ratio
                    localStorage.setItem('ratio', +e.target.getAttribute('data-ratio')); //теперь в локальном хранилище будет сохранятся коэффициент

                } else {
                    sex = e.target.getAttribute('id');
                    localStorage.setItem('sex', e.target.getAttribute('id')); //теперь в локальном хранилище будет сохранятся выбранный пол

                }

                elements.forEach(elem => {
                    elem.classList.remove(activeClass); //сначала класс активности убираему всех элементов
                });
                e.target.classList.add(activeClass); //к выбранному элементу добавляем класс активности

                calcTotal();
            });
        })
    }

    getStaticInformation('#gender div', 'calculating__choose-item_active');
    getStaticInformation('.calculating__choose_big div', 'calculating__choose-item_active');

    function getDynamicInformation(selector) {  //selector - это селектор инпутов, в кот будут записываться значения
        const input = document.querySelector(selector);

        input.addEventListener('input', () => {  //что мы будем делать, когда пользователь что-то вводит

            if (input.value.match(/\D/g)) {   //проверка на введение цифр пользователем
                input.style.border = '1px solid red';
            } else {
                input.style.border = 'none';
            }

            switch(input.getAttribute('id')) {  //смотря куда пользователь вводит значение, будем брать это значение и присваивать соответствующей переменной
                case 'height':
                    height = +input.value;
                    break;
                case 'weight':
                    weight = +input.value;
                    break;
                case 'age':
                    age = +input.value;
                    break;
            }
            calcTotal();
        });
    }
    getDynamicInformation('#height');
    getDynamicInformation('#weight');
    getDynamicInformation('#age');
}


/***/ }),

/***/ "./js/modules/cards.js":
/*!*****************************!*\
  !*** ./js/modules/cards.js ***!
  \*****************************/
/*! exports provided: cards */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cards", function() { return cards; });
function cards() {
    //Используем классы для карточек. Шаблонизируем
    class MenuCard {
        constructor(src, alt, title, desc, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.desc = desc;
            this.price = price;
            this.parent = document.querySelector(parentSelector);
            this.classes = classes;
            this.transfer = 70; //курс доллара
            this.changeToRUB();
        }

        changeToRUB() {
            this.price = this.price * this.transfer
        }

        render() {
            const element = document.createElement('div');
            if (this.classes.length === 0) {   //если у элемента нет массива с классами, то добавляем класс по умолчанию menu__item
                this.element = 'menu__item';
                element.classList.add(this.element);
            } else {
                this.classes.forEach(className => element.classList.add(className));  //Иначе каждый имеющийся в массиве классов класс добавляем к элементу
            }


            this.classes.forEach(className => element.classList.add(className))   //каждый элемент внутри массива это className. Добавляем к элементу каждый новый класс
            element.innerHTML = `
                    <img src=${this.src} alt=${this.alt}>
                    <h3 class="menu__item-subtitle">${this.title}</h3>
                    <div class="menu__item-descr">${this.desc}</div>
                    <div class="menu__item-divider"></div>
                    <div class="menu__item-price">
                        <div class="menu__item-cost">Цена:</div>
                        <div class="menu__item-total"><span>${this.price}</span> руб/день</div>
                    </div>
            `;
            this.parent.append(element);
        }
    }

    const getResource = async (url) => {  //функция настраивает запрос, посылает его, ожидает ответ от сервера и после трансформирует ответ в json. Это асинхронный код. Функция fetch ничего не будет получать. Поэтому надо дождаться результата промиса. Воспользуемся async await
        const res = await fetch(url);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}, status: ${res.status}`);
        }

        return await res.json();  //здесь возвращается промис и его окончания работы нужно дождаться
    };

    // getResource('http://localhost:3000/menu')  //для вывода карточек
    //     .then(data => {
    //        data.forEach(({img, altimg, title, descr, price}) => {
    //          new MenuCard(img, altimg, title, descr, price, '.menu .container').render();
    //        });
    //     });

    // getResource('http://localhost:3000/menu')  //для вывода карточек. функция получает data - массив
    //     .then(data => createCard(data));  //

    axios.get('http://localhost:3000/menu')
        .then(data => {
            data.data.forEach(({img, altimg, title, descr, price}) => {
                new MenuCard(img, altimg, title, descr, price, '.menu .container').render();
            });
        });

    function createCard(data) {
        data.forEach(({img, altimg, title, descr, price}) => {   //массив data перебирается через фор ич. Объекты деструктуризируются на отдельные св-ва
            price *= 70;
            const element = document.createElement('div');

            element.classList.add('menu__item');

            element.innerHTML = ` 
                <img src=${img} alt=${altimg}>
                <h3 class="menu__item-subtitle">${title}</h3>
                <div class="menu__item-descr">${descr}</div>
                <div class="menu__item-divider"></div>
                <div class="menu__item-price"
                    <div class="menu__item-cost">Цена:</div>
                    <div class="menu__item-total"><span>${price}</span> руб/день</div>
                </div>
            `;

            document.querySelector('.menu .container').append(element);
        })
    }

}


/***/ }),

/***/ "./js/modules/forms.js":
/*!*****************************!*\
  !*** ./js/modules/forms.js ***!
  \*****************************/
/*! exports provided: forms */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "forms", function() { return forms; });
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal */ "./js/modules/modal.js");


function forms() {
    //AJAX Forms
    //задача: взять несколько форм на сайте и отправить данные из форм в файл server.php
    const forms = document.querySelectorAll('form');

    const message = {
        loading: '/img/form/spinner.svg',
        success: 'Спасибо! Скоро мы с Вами свяжемся',
        failure: 'Что-то пошло не так...'
    }

    forms.forEach(item => {
        bindPostData(item);
    });

    const postData = async (url, data) => {  //функция настраивает запрос, посылает его, ожидает ответ от сервера и после трансформирует ответ в json. Это асинхронный код. Функция fetch ничего не будет получать. Поэтому надо дождаться результата промиса. Воспользуемся async await
        const res = await fetch(url, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: data
        });

        return await res.json();  //здесь возвращается промис и его окончания работы нужно дождаться
    };

    function bindPostData(form) {
        form.addEventListener('submit', (e) => {
            e.preventDefault(); //отменяем стандартное поведение браузера - перезагрузку страницы после нажатия кнопки submit

            const statusMessage = document.createElement('img');
            statusMessage.src = message.loading; //создали изображение и подставили атрибут src из const message loading
            statusMessage.style.cssText = `
                display: block;
                margin: 0 auto;
            `;
            form.append(statusMessage);
            form.insertAdjacentElement('afterend', statusMessage); //вставляем спиннер после формы

            //Для JSON нужен заголовок
            const formData = new FormData(form);  //объект, кот позволяет быстро сформировать данные, полученные из формы в формате ключ - значение

            //есть объект специфический FormData, который нужно преобразовать в JSON. Просто так сделать это нельзя, а можно:
            const json = JSON.stringify(Object.fromEntries(formData.entries())); //получаем массив с массивами, после этого в норм объект, а после в JSON

            postData('  http://localhost:3000/requests', json)
                .then(data => {
                    console.log(data);
                    showThanksModal(message.success); //показывается мод окно и через 4 сек оно будет закрываться вмсете с возвращением норм контента
                    form.reset(); //после успешной отправки очищаем форму
                    statusMessage.remove();
                }).catch(() => {
                showThanksModal(message.failure);
            }).finally(() => {
                form.reset();
            });
        });
    }

    function showThanksModal(message) {   //функция, скрывающая модальное окно и показывающая сообщение об успешной отправке данных
        const prevModalDialog = document.querySelector('.modal__dialog');

        prevModalDialog.classList.add('hide'); //скрыли модальное окно
        openModal();

        const thanksModal = document.createElement('div');
        thanksModal.classList.add('modal__dialog');
        thanksModal.innerHTML = `
            <div class="modal__content">
                <div class="modal__close" data-close>×</div>
                <div class="modal__title">${message}</div> 
                <!-- то сообщение, которое в массиве message -->
            </div>
        `;

        document.querySelector('.modal').append(thanksModal);
        setTimeout(() => {
            thanksModal.remove();
            prevModalDialog.classList.add('show');
            prevModalDialog.classList.remove('hide');
            closeModal();
        }, 4000);
    }

    fetch('http://localhost:3000/menu')//копируем из терминала после запуска json-server db.json
        .then(data => data.json())
        .then(res => console.log(res));  //получаем данные из файла db.json

    //Fetch API
    // fetch('https://jsonplaceholder.typicode.com/posts', {
    //     method: "POST",
    //     body: JSON.stringify({name: 'Alex'}),
    //     headers: {
    //         'Content-type': 'application/json'
    //     }
    // })
    //         .then(response => response.json())  //что-то приодит от сервера. превращает json в обычный объект promice
    //         .then(json => console.log(json));
}


/***/ }),

/***/ "./js/modules/modal.js":
/*!*****************************!*\
  !*** ./js/modules/modal.js ***!
  \*****************************/
/*! exports provided: openModal, closeModal, modal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openModal", function() { return openModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "closeModal", function() { return closeModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "modal", function() { return modal; });
function openModal() {
    modal.classList.add('show');
    modal.classList.remove('hide');
    document.body.style.overflow = 'hidden'; //убираем вертикальный скроллинг на странице, так как открыто мод окно
    clearInterval(modalTimerId)  //если пользователь сам открыл мод окно, то оно не должно ему показываться через несколько секунд. Поэтому сбрасываем счетчик
}

function closeModal() {
    modal.classList.add('hide');
    modal.classList.remove('show');
    document.body.style.overflow = ''; //возвращаем странице скроллинг
}

function modal() {
    //Modal window

    const modalTrigger = document.querySelectorAll('[data-modal]'), //Кнопки "связаться с нами" (псевдомассив кнопок. Далее перебираем фор ичем)
        modal = document.querySelector('.modal');  //само модальное окно
    // modalCloseBtn = document.querySelector('[data-close');  //крестик в мод окне

    modalTrigger.forEach(btn => {
        btn.addEventListener('click', openModal);
    });

    // modalCloseBtn.addEventListener('click', closeModal);

    //показывать и скрывать мод окно через тогл:

    // modalTrigger.addEventListener('click', () => {
    //     modal.classList.toggle('show');
    //     document.body.style.overflow = 'hidden'; //убираем вертикальный скроллинг на странице, так как открыто мод окно
    // });
    //
    // modalCloseBtn.addEventListener('click', () => {
    //     modal.classList.toggle('show');
    //     document.body.style.overflow = ''; //возвращаем странице скроллинг
    // });


    modal.addEventListener('click', (e) => {
        if (e.target === modal || e.target.getAttribute('data-close') == '') {    //если клик на область с классом модал, то закрываем мод окно
            closeModal();
        }
    });

    document.addEventListener('keydown', (e) => {  //закрытие мод окна по нажатию по ESC
        if (e.code === "Escape" && modal.classList.contains('show')) {  //если нажата ESC и модал окно содержит класс шоу, то есть открыто, то:
            closeModal();
        }
    });

    const modalTimerId = setTimeout(openModal, 50000); //если пользователь не открыл мод окно, то оно само откроется через 50 сек

    //чтобы мод окно показывалось после пролистывания страницы вниз до конца:

    function showModalByScroll() {
        if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight){ //берем прокрутку справа + тот контент, кот сейчас видим, сравниваемс полным сайтом. как только они совпадают, то значит клиент долистал до конца
            openModal();
            window.removeEventListener('scroll', showModalByScroll); //чтобы модальное окно открылось только один раз при пролитсывании в конец страницы
        }
    }

    window.addEventListener('scroll', showModalByScroll);
}



/***/ }),

/***/ "./js/modules/slider.js":
/*!******************************!*\
  !*** ./js/modules/slider.js ***!
  \******************************/
/*! exports provided: slider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slider", function() { return slider; });
function slider() {
    //Slider
    const slides = document.querySelectorAll('.offer__slide'), //сами картинки
        prev = document.querySelector('.offer__slider-prev'),  //стрелка назад
        next = document.querySelector('.offer__slider-next'),  //стрелка вперед
        total = document.querySelector('#total'),   //всего слайдов
        current = document.querySelector('#current');  //номер текущего слайда

    //для второго варианта слайдера:
    const slideWrapper = document.querySelector('.offer__slider-wrapper'),  //Обертка картинки
        slidesField = document.querySelector('.offer__slider-inner'),  //блок с внутренним содержанием (картинки)
        width = window.getComputedStyle(slideWrapper).width; //получаем только ширину обертки слайда

    //Navigation for slider
    const slider = document.querySelector('.offer__slider');

    let slideIndex = 1; //let так как переменная в дальнейшем будет меняться

    //LIGHT SLIDER:
    //
    // showSlides(slideIndex); //изначально слайд показывает первую картинку
    //
    // if (slides.length < 10) {
    //     total.textContent = `0${slides.length}`;  //показ общего количества слайдов
    // } else {
    //     total.textContent = slides.length;
    // }
    //
    // function showSlides(n) {   //n - слайд индекс
    //     if (n > slides.length) {
    //         slideIndex = 1; //если n > количество слайдов (перещелкали), то переходим на первый слайд
    //     }
    //     if (n < 1) {
    //         slideIndex = slides.length; //если номер слайда меньше 1, то переходим на последний слайд
    //     }
    //     slides.forEach(item => item.style.display = 'none'); //все слайды скрыты
    //     slides[slideIndex - 1].style.display = 'block'; //показываем выбранный слайд
    //
    //     if (slides.length < 10) {
    //         current.textContent = `0${slideIndex}`;  //показ номера текущего слайда
    //     } else {
    //         current.textContent = slideIndex[slideIndex - 1];
    //     }
    //
    // }
    //
    // function plusSlides(n) {
    //     showSlides(slideIndex += n);  //вызываем функцию показа слайдов и передаем в параметрах индекс слайда. Если +1, то будет показываться след слайд, если -1, то slideIndex = slideIndex - 1
    // }
    //
    // prev.addEventListener('click', () => {
    //     plusSlides(-1);   //обработчик события на нажатие на стрелку "пред. слайд".
    // });
    //
    // next.addEventListener('click', () => {
    //     plusSlides(1);   //обработчик события на нажатие на стрелку "след. слайд".
    // });


    //HARD SLIDER:
    let offset = 0; //отступ

    if (slides.length < 10) {
        total.textContent = `0${slides.length}`;  //показ общего количества слайдов
        current.textContent = `0${slideIndex}`;
    } else {
        total.textContent = slides.length;
        current.textContent = slideIndex;
    }

    slidesField.style.width = 100 * slides.length + '%';  //так мы сможем поместить все слайды на странице в один блок
    slidesField.style.display = 'flex';  //делаем, чтобы все картинки располагались в ряд
    slidesField.style.transition = '0.5s all';

    slideWrapper.style.overflow = 'hidden';  //показываем только одну картинку

    slides.forEach(slide => {
        slide.style.width = width; //каждому слайду устанавливаем одну и ту же ширину
    });

    slider.style.position = 'relative';  //все элементы, кот спозиционированы абсолютно, теперь будут нормально отображаться

    const indicators = document.createElement('ol'), //обертка для всех точек-индикаторов
        dots = []; //массив всех индикаторов

    indicators.classList.add('carousel-indicators');  //обертка для индикаторов
    indicators.style.cssText = `
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 15;
        display: flex;
        justify-content: center;
        margin-right: 15%;
        margin-left: 15%;
        list-style: none;
    `;
    slider.append(indicators); //помещаем обертку индикаторов внутрь слайдера

    for (let i = 0; i < slides.length; i++) {  //создаем столько точек, сколько слайдов
        const dot = document.createElement('li');
        dot.setAttribute('data-slide-to', i + 1); //выставляем каждой точке атрибут с соответствующим номером слайда
        dot.style.cssText = `
            box-sizing: content-box;
            flex: 0 1 auto;
            width: 30px;
            height: 6px;
            margin-right: 3px;
            margin-left: 3px;
            cursor: pointer;
            background-color: #fff;
            background-clip: padding-box;
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent;
            opacity: .5;
            transition: opacity .6s ease;
        `;
        if (i == 0) {   //первая точка будет ярче
            dot.style.opacity = '1';
        }
        indicators.append(dot); //помещаем точки в обертку индикаторов
        dots.push(dot); //помещаем точку в массив
    }

    function getZeroSlides(slides) {
        if (slides.length < 10) {
            return current.textContent = `0${slideIndex}`; //если слайдов меньше 10, то славим ноль перед номером слайда
        } else {
            return current.textContent = slideIndex; //если слайдов > 10, то не ставим ноль перед номером слайда
        }
    }

    function dotsOpacity() {
        dots.forEach(dot => dot.style.opacity = '.5'); //изначально у каждой точки прозрачность 50%
        dots[slideIndex - 1].style.opacity = '1';  //точка номер = текущего слайда ярче
    }

    function deleteNotDigits(str) {
        return +str.replace(/\D/g, '');
    }

    //нажатие на стрелку вправо
    next.addEventListener('click', () => {  //здесь проверяем на последний слайд
        if (offset === deleteNotDigits(width) * (slides.length - 1)) {  //изначально width - это напрмер "500px". Превращаем ее в число (+) и все не числа заменяем на ''. Таким образом, получаем только цифры.
            offset = 0;   //если последний слайд
        } else {  //если не последний слайд
            offset += deleteNotDigits(width)
        }

        slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

        if (slideIndex == slides.length) {  //значит мы дошли до конца слайдера
            slideIndex = 1  //перемещаемся на первую позицию
        } else {
            slideIndex++;
        }

        getZeroSlides(slides);
        dotsOpacity();

    });

    //нажатие на стрелку влево
    prev.addEventListener('click', () => {  //здесь проверяем на последний слайд
        if (offset == 0) {  //если первый слайд
            offset = deleteNotDigits(width) * (slides.length - 1); //изначально width - это напрмер "500px". Превращаем ее в число (+) и отрезаем "px"
        } else {
            offset -= deleteNotDigits(width)
        }

        slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

        if (slideIndex == 1) {  //когда мы находимся на первом слайде, клие на кнопку предыдущего слайда будет смещать нас на последний слайд
            slideIndex = slides.length
        } else {
            slideIndex--;  //когда нажимаем на кнопку предыдущий слайд, то показываем предыдущий слайд
        }

        getZeroSlides(slides);
        dotsOpacity();
    });

    //добавляем возмоность попадать на нужный слайд, нажимая на индикаторы
    dots.forEach(dot => {
        dot.addEventListener('click', (e) => {
            const slideTo = e.target.getAttribute('data-slide-to');

            slideIndex = slideTo;
            offset = deleteNotDigits(width) * (slideTo - 1);
            slidesField.style.transform = `translateX(-${offset}px)`;  //когда нажимаем на стрелку вправо, то сдвигаем картинки влево

            getZeroSlides(slides);
            dotsOpacity();
        });
    });

}


/***/ }),

/***/ "./js/modules/tabs.js":
/*!****************************!*\
  !*** ./js/modules/tabs.js ***!
  \****************************/
/*! exports provided: tabs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tabs", function() { return tabs; });
function tabs() {
    //Табы или слайдер
    const tabs = document.querySelectorAll('.tabheader__item'),  //элемент меню справа
        tabsContent = document.querySelectorAll('.tabcontent'),  //картинки слайдера
        tabsParent = document.querySelector('.tabheader__items'); //меню справа

    function hideTabContent() {
        tabsContent.forEach(item => {
            // item.style.display = 'none';
            item.classList.add('hide');
            item.classList.remove('show', 'fade');
        });

        tabs.forEach(item => {
            item.classList.remove('tabheader__item_active');
        });
    }

    function showTabContent(i = 0) {
        // tabsContent[i].style.display = "block";
        tabsContent[i].classList.add('show', 'fade');
        tabsContent[i].classList.remove('hide');
        tabs[i].classList.add('tabheader__item_active');
    }

    hideTabContent();
    showTabContent(); //если написать в функции во входных параметрах, например, i = 0, то когда вызываем функцию без аргумента, то по умолчанию будет и=0

    tabsParent.addEventListener('click', (event) => {
        const target = event.target;

        if (target && target.classList.contains('tabheader__item')) {
            tabs.forEach((item, i) => {
                if (target == item) {
                    hideTabContent();
                    showTabContent(i);
                }
            });
        }
    });
}


/***/ }),

/***/ "./js/modules/timer.js":
/*!*****************************!*\
  !*** ./js/modules/timer.js ***!
  \*****************************/
/*! exports provided: timer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timer", function() { return timer; });
function timer() {
    //Обратный отсчет времени
    const deadline = '2020-12-01';

    //функция разницы между deadline и текущим временем
    function getTimeRemaining(endtime) {
        const t = Date.parse(endtime) - Date.parse(new Date()), //разница в миллисекундах между датой будущего события и текущей датой
            //чтобы получить из t количество дней, делим t на количество миллисекунд в часе и округляем полученное значение:
            days = Math.floor(t / (1000 * 60 * 60 * 24)),
            hours = Math.floor((t / (1000 * 60 * 60) % 24)), //получаем хвостик, которого не хватает до полных суток
            minutes = Math.floor((t / 1000 /60) % 60),  //получаем хвостик, которого не хватает до полного часа
            seconds = Math.floor((t / 1000) % 60);

        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,  //получаем объект
        };
    }

    function getZero(num) {
        if (num >= 0 && num < 10) {
            return `0${num}`;
        } else {
            return num;
        }
    }

    function setClock(selector, endtime) {
        const timer = document.querySelector(selector),
              days = timer.querySelector('#days'),
              hours = timer.querySelector('#hours'),
              minutes = timer.querySelector('#minutes'),
              seconds = timer.querySelector('#seconds'),
              timeInterval = setInterval(updateClock, 1000); //запуск фнкции updateClock через каждую секунду

        updateClock(); //вызываем эту функцию, чтоюы значения дней, часов, сек и миллисек, которые стоят в верстке, не отображались

        function updateClock() {
            const t = getTimeRemaining(endtime);  //сюда записывается результат работы функции getRem.. в качестве объекта

            days.innerHTML = getZero(t.days);
            hours.innerHTML = getZero(t.hours);
            minutes.innerHTML = getZero(t.minutes);
            seconds.innerHTML = getZero(t.seconds);

            if (t.total <= 0) {
                clearInterval(timeInterval); //если время вышло, то таймер не обновляем
            }
        }
    }

    setClock('.timer', deadline);
}


/***/ }),

/***/ "./js/script.js":
/*!**********************!*\
  !*** ./js/script.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_tabs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/tabs */ "./js/modules/tabs.js");
/* harmony import */ var _modules_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/modal */ "./js/modules/modal.js");
/* harmony import */ var _modules_timer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/timer */ "./js/modules/timer.js");
/* harmony import */ var _modules_cards__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/cards */ "./js/modules/cards.js");
/* harmony import */ var _modules_calc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/calc */ "./js/modules/calc.js");
/* harmony import */ var _modules_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/forms */ "./js/modules/forms.js");
/* harmony import */ var _modules_slider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/slider */ "./js/modules/slider.js");








window.addEventListener('DOMContentLoaded', () => {
//
//     // const tabs = require('./modules/tabs'),
//     //       modal = require('./modules/modal'),
//     //       timer = require('./modules/timer'),
//     //     cards = require('./modules/cards'),
//     //     calc = require('./modules/calc'),
//     //     forms = require('./modules/forms'),
//     //     slider = require('./modules/slider');
//
    Object(_modules_tabs__WEBPACK_IMPORTED_MODULE_0__["tabs"])();
    Object(_modules_modal__WEBPACK_IMPORTED_MODULE_1__["modal"])();
    Object(_modules_timer__WEBPACK_IMPORTED_MODULE_2__["timer"])();
    Object(_modules_cards__WEBPACK_IMPORTED_MODULE_3__["cards"])();
    Object(_modules_calc__WEBPACK_IMPORTED_MODULE_4__["calc"])();
    Object(_modules_forms__WEBPACK_IMPORTED_MODULE_5__["forms"])();
    Object(_modules_slider__WEBPACK_IMPORTED_MODULE_6__["slider"])();
 });







/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map